<!DOCTYPE html>
<html>
<body>

<?php
echo "My first PHP script!";

$num = 10;

if( $num > 2) {
	echo $num;
} else {
	echo 'hi';
}

function printHelo(){
	echo 'hello';
}
printHelo();

// function
function nama_kapital($nama) {
	return ucwords($nama);
}
echo "<br>";
$nama_lengkap = nama_kapital("faris");
echo $nama_lengkap;
echo "Halloo semua saya $nama_lengkap";

//looping
for($i = 0; $i<5 ; $i++){
	echo "ini adalah looping ke $i" ."<br>";
}
//whike
$x = 1;

while ($x<= 5) {
	echo "$x <br>";
    $x ++;
}

// for each loop
$animals = ["cat", "dog", "snake", "ant", "lion"];
foreach ($animals as $animal){
	echo "$animal <br>";
}

$age = array("Rezky"=>"25", "Abduh"=>"29", "Iqbal"=>"33");

foreach($age as $x => $val) {
  echo "$x = $val<br>";
}

?> 

</body>
</html>
