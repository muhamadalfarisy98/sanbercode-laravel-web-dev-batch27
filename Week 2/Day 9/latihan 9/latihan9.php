<!DOCTYPE html>
<html>
<body>

<?php
echo "My first PHP script!";

class Mobil 
{
  public $roda = 4;
  function jalan(){
    echo "Mobil berjalan";
  }
  function jumlah_roda(){
  	echo $this->roda;
  }
} 
//membuat object
$mini = new Mobil();
$mini->jalan(); // menampilkan echo 'Mobil berjalan'
//echo $mini->roda; // 4
$mini->jumlah_roda();

// visibilitas
echo 'tes1';
// inheritance protected
class Mobilbaru 
{
  protected $roda = 4;

}

class MobilSport extends Mobilbaru
{
  protected $maxSpeed;
}

$ferrari = new MobilSport();
//echo $ferrari->roda ; // 4 
echo 'tes2';


//public
class Mobillama
{
  private $roda = 4;
  public function jumlahRoda()
  {
    echo $this->roda;
  }
}

$kijang = new Mobillama();
$kijang->jumlahRoda(); // menampilkan 4
echo 'tes3';

//constructor

class Mobilcanggih {
  protected $roda= 4;
  public $merk;
  public function __construct($merk) 
  {
    $this->merk= $merk;
  }
}

$xeniya = new Mobilcanggih("Xeniya");
echo $xeniya->merk; // Xeniya


?> 

</body>
</html>
