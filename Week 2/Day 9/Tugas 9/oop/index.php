<?php
require('animal.php');
require('Ape.php');
require('Frog.php');

// RELEASE 0
// Animal 
$sheep = new Animal("shaun");

echo 'Name: '. $sheep->name . "<br>"; // "shaun"
echo 'legs: '.$sheep->legs . "<br>"; // 4
echo 'cold blooded: '.$sheep->cold_blooded . "<br><br>"; // "no"

// RELEASE 1

// Frog 
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo 'Name: '. $kodok->name . "<br>"; // "shaun"
echo 'legs: '.$kodok->legs . "<br>"; // 4
echo 'cold blooded: '.$kodok->cold_blooded . "<br>"; // "no"
echo 'Jump: '. $kodok->jump(). "<br><br>";


// Ape 
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo 'Name: '. $sungokong->name . "<br>"; // "shaun"
echo 'legs: '.$sungokong->legs . "<br>"; // 4
echo 'cold blooded: '.$sungokong->cold_blooded . "<br>"; // "no"
echo 'Yell: '. $sungokong->yell(). "<br><br>";

?> 