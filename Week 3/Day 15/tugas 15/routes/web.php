<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'IndexController@home' );
Route::get('/form', 'FormController@biodata' );
Route::get('/table', 'TableController@table' );



// CRUD
Route::get('/cast', 'CastController@index' );
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
// show by id 
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');





// Route::get('/test')
Route::get('/data-table', function(){
    return view('table.data-table');
});

Route::get('/test', function(){
    return view('layout.master');
});