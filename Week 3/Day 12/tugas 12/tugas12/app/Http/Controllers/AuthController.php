<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function form(){
        return view('pages.form');
    }

    public function kirim(Request $request){
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];

        // dd($request->all()); 

        // compact -> lempar variable 
        return view('pages.welcome', compact('first_name','last_name'));

    }
}
