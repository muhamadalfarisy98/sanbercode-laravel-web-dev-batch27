@extends('layout.master')
@section('judul')
    Halaman Form
@endsection

@section('isi')
<form action="kirim" method="post">
    @csrf
    <p>First name:</p>
    <input type="text" name="first_name" required>
    <p>Last name:</p>
    <input type="text" name="last_name" required>

    <p>Gender:</p>
    <input type="radio" name="gender" value="male">Male<br>
    <input type="radio" name="gender" value="female">Female<br>
    <input type="radio" name="gender" value="other">Other<br>
    
    <p>Nationality:</p>
    <select name="country">
        <option value="1">Indonesian</option>
        <option value="2">Singaporean</option>
        <option value="3">Malaysian</option>
        <option value="4">Australian</option>
    </select>

    <p>Language Spoken:</p>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Other<br>

    <p>Bio:</p>
    <textarea name="bio" cols="50" rows="20"></textarea>
    <br>
    <button type="submit">Sign Up</button>
</form>    
@endsection
