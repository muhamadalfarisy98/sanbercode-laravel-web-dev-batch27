@extends('layout.master')
@section('judul')
    Halaman edit film {{$film->judul}}
@endsection

@section('isi')
    <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">Nama Film</label>
            <input type="text" class="form-control" name="judul" id="judul" value="{{$film->judul}}" placeholder="Masukkan nama cast">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">Ringkasan Film</label>
            {{-- <input type="text" class="form-control" name="ringkasan" id="title" placeholder="Masukkan nama cast"> --}}
            <textarea name="ringkasan" id="ringkasan" class="form-control" cols="30" value="{{$film->ringkasan}}" rows="10"></textarea>
            @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="tahun">Tahun rilis</label>
            <input type="text" class="form-control" name="tahun" id="tahun" value="{{$film->tahun}}" placeholder="Masukkan nama cast">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="genre_id">Genre film</label>
            {{-- <input type="text" class="form-control" name="genre_id" id="genre_id" placeholder="Masukkan nama cast"> --}}
            <select name="genre_id" id="genre_id" class="form-control">
                <option value="" >..Pilih genre..</option>
                @foreach ($genre as $item)
                @if ($item->id === $film->genre_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
                    
                @endforeach
            </select>
            
            @error('genre_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <div class="form-group">
            <label for="title">Poster Film</label>
            <input type="file" class="form-control" name="poster" id="poster" placeholder="Masukkan nama cast">
            @error('poster')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection