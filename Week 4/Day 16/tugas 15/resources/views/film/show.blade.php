@extends('layout.master')
@section('judul')
    Halaman Detail film {{$film->judul}}
@endsection

@section('isi')
    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="{{asset('poster/'. $film->poster)}}" alt="Card image cap">
        <div class="card-body">
        <h2 class="card-title">{{$film->judul}} ({{$film->tahun}})</h2>
        <p class="card-text">{{$film->ringkasan}}</p>
        </div>
    </div>

@endsection