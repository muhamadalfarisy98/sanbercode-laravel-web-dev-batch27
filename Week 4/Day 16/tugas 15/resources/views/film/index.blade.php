@extends('layout.master')
@section('judul')
    Halaman list film
@endsection


@section('isi')
    <a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>
    <div class="row">
        @foreach ($film as $item)
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="{{asset('poster/'. $item->poster)}}" alt="Card image cap">
                    <div class="card-body">
                    <h2 class="card-title">{{$item->judul}} ({{$item->tahun}})</h2>
                    <p class="card-text">{{$item->ringkasan}}</p>
                    
                    {{-- <a href="/film/{{$item->id}}" class="btn btn-danger">Delete</a> --}}
                    <form action="/film/{{$item->id}}" method="POST">
                        <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                        <a href="/film/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                    </div>
                </div>
            </div>
        @endforeach
        
        

    </div>


@endsection