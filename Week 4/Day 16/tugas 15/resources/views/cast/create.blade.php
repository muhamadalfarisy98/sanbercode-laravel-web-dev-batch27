@extends('layout.master')
@section('judul')
    Halaman List Cast
@endsection

@section('isi')
    {{-- CREATE CAST  --}}

    {{-- fungsi postnya disesuaikan dengan yg diroute 
        methodnya post, actionnya /cast (routing ke sini) sesudah submit ditekan
        --}}
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Nama Cast</label>
            <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan nama cast">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body_umur">Umur</label>
            <input type="text" class="form-control" name="umur" id="body_umur" placeholder="Masukkan umur">
            {{-- VALIDASI  --}}
            {{-- bootstrap 4 alerts  --}}
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            <label for="body_bio">Bio</label>
            <br>
            {{-- <input type="text" class="form-control" name="bio" id="body_bio" placeholder="Masukkan bio"> --}}
            <textarea name="bio" id="body_bio" cols="20" rows="6" required placeholder="Masukkan bio"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>

@endsection