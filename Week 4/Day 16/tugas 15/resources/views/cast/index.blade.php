@extends('layout.master')
@section('judul')
    Halaman List Cast
@endsection

@section('isi')
    
    <a href="/cast/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            {{-- <th scope="col">id</th> --}}
            <th scope="col">Nama cast</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
            {{-- ambil yg kita lempar tadi (compact)- --}}
            @forelse ($cast as $key=>$value)
                {{-- key -> index counter dimulai dari nol  --}}
                <tr>
                    <td>{{$key + 1}}</th>
                    {{-- <td>{{$value->id}}</th> --}}
                    <td>{{$value->nama}}</td>
                    <td>{{$value->umur}}</td>
                    <td>{{$value->bio}}</td>
                    <td>
                        <form action="/cast/{{$value->id}}" method="POST">
                            <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection
