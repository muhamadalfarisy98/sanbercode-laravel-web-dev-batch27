@extends('layout.master')
@section('judul')
    Halaman Edit cast id : {{$cast->id}}
@endsection

@section('isi')

    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Nama Cast</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}"  id="title" placeholder="Masukkan nama cast">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>

@endsection