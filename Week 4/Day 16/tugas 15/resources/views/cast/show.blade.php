@extends('layout.master')
@section('judul')
    Halaman detail cast id {{$cast->id}}
@endsection

@section('isi')
{{-- <h2>Show Post {{$cast->id}}</h2> --}}
<h4>Nama Cast: {{$cast->nama}}</h4>
<p>Umur: {{$cast->umur}}</p>
<p>Bio: {{$cast->bio}}</p>
@endsection
