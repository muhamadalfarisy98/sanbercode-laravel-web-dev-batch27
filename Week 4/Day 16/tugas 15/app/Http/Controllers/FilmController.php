<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Film; // panggil model
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // all berfungsi mengambil semua data di database fitur eloquent dengan method all()
        $film = Film::all();
        return view('film.index', compact('film'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $genre = DB::table('genre')->get();
        return view('film.view', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
    		'judul' => 'required',
    		'ringkasan' => 'required',
            'poster' => 'mimes:jpeg,jpg,png|max:2200',
            'genre_id' => 'required',
            'tahun' => 'required',
    	]);

        $poster = $request->poster;
        // agar namanya uniq, ngambil nama asli dari filenya
        $new_poster = time() .  ' - ' . $poster->getClientOriginalName();

 
        Film::create([
    		'judul' => $request->judul,
    		'ringkasan' => $request->ringkasan,
            'poster' => $new_poster,
            'genre_id' => $request->genre_id,
            'tahun' => $request->tahun,
    	]);
        //simpan gambar di public/poster
        $poster->move('poster/', $new_poster);

    	return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // fitur eloquent find()
        $film = Film::find($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $film = Film::find($id);
        $genre = DB ::table('genre')->get();
        return view('film.edit', compact('film'),compact('genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
    		'judul' => 'required',
    		'ringkasan' => 'required',
            'poster' => 'mimes:jpeg,jpg,png|max:2200',
            'genre_id' => 'required',
            'tahun' => 'required',
    	]);

        $film = Film::findorfail($id);

        if ($request->has('poster')){
            $path = "poster/";
            File::delete($path . $film->poster);
            $poster = $request->poster;
            $new_poster = time() . ' - ' . $poster->getClientOriginalName();
            $poster->move('poster/', $new_poster);
            $film_data = [
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'poster' => $new_poster,
                'genre_id' => $request->genre_id,
                'tahun' => $request->tahun,
            ];
        } else{
            $film_data = [
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'genre_id' => $request->genre_id,
                'tahun' => $request->tahun,
            ];
        }

        $film->update($film_data);
        return redirect('/film');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $film = Film::findorfail($id);
        $film->delete();
        $path = 'poster/';
        File::delete($path . $film->poster);

        return redirect('/film');
    }
}
