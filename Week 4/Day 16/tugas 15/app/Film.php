<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    //mengarahkan model ke table film di database
    protected $table = "film";

    // kolom apa saja dari table film yang ingin dimanipulasi
    protected $fillable = ["judul", "ringkasan","poster","genre_id","tahun"];
}
